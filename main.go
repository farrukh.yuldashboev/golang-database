package main

import (
	"fmt"
	"log"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

var schema = `

CREATE TABLE IF NOT EXISTS "author" (
    "id" SERIAL PRIMARY KEY,
    "firstname" varchar(255) NOT NULL,
    "lastname" varchar(255) NOT NULL,
    "created_at" TIMESTAMP DEFAULT(Now()),
    "updated_at"  TIMESTAMP DEFAULT(Now())
);

CREATE TABLE IF NOT EXISTS "article" (
    "id" SERIAL PRIMARY KEY,
    "title" VARCHAR(255) NOT NULL UNIQUE,
    "body" TEXT,
    "author_id" INT,
    "created_at" TIMESTAMP DEFAULT(Now()),
    "updated_at"  TIMESTAMP DEFAULT(Now()),
    CONSTRAINT fk_author FOREIGN KEY(author_id) REFERENCES author(id)
);

INSERT INTO author (firstname, lastname) VALUES ('Jason', 'Moiron') ON CONFLICT DO NOTHING;
INSERT INTO author (firstname, lastname) VALUES ('John', 'Doe') ON CONFLICT DO NOTHING;

INSERT INTO article (title, body, author_id) VALUES ('Lorem1', 'Lorem ipsum1', 1) ON CONFLICT DO NOTHING;
INSERT INTO article (title, body, author_id) VALUES ('Lorem2', 'Lorem ipsum2', 2) ON CONFLICT DO NOTHING;
`

type Content struct {
	Title string `json:"title"`
	Body  string `json:"body"`
}

type Article struct {
	ID        int        `json:"id"`
	Content              // Promoted fields
	Author    Person     `json:"author"` // Nested structs
	CreatedAt *time.Time `json:"-"`
}

type Person struct {
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
}

func main() {
	psqlConnString := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		"localhost",
		5432,
		"postgres",
		"admin",
		"bootcamp",
	)

	db, err := sqlx.Connect("postgres", psqlConnString)
	if err != nil {
		log.Panic(err)
	}

	db.MustExec(schema)

	rows, err := db.Query(
		"SELECT ar.id, ar.title, ar.body, ar.created_at, au.firstname, au.lastname FROM article AS ar JOIN author AS au ON ar.author_id = au.id WHERE ar.id <= $1",
		10,
	)
	if err != nil {
		log.Panic(err)
	}
	defer rows.Close()

	var arr []Article
	for rows.Next() {
		var a Article
		err = rows.Scan(&a.ID, &a.Title, &a.Body, &a.CreatedAt, &a.Author.Firstname, &a.Author.Lastname)
		arr = append(arr, a)
		if err != nil {
			log.Panic(err)
		}
	}

	fmt.Println(arr)

	res, err := db.NamedExec(
		`INSERT INTO article (title, body, author_id) VALUES (:t, :b, :a_id)`,
		map[string]interface{}{
			"t":    "Bin",
			"b":    "Smuth",
			"a_id": 2,
		},
	)

	if err != nil {
		log.Println("----->", err)
	}

	fmt.Printf("%#v", res)

	res2, err2 := db.Exec(
		`UPDATE article SET title=$1, body=$2, updated_at=now() WHERE id = $3`,
		"1111",
		"22222222",
		3,
	)

	if err2 != nil {
		log.Panic(err2)
	}

	num, err := res2.RowsAffected()
	if err2 != nil {
		log.Panic(err2)
	}
	fmt.Println("affected rows --->", num)

	//delete column
	res3, err3 := db.Exec(`DELETE FROM article WHERE id = 3;`)
	if err3 != nil {
		fmt.Println("err delete --->", err3)
	}
	num, err = res3.RowsAffected()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("affected rows ---> delete", num)

	//add column
	res3, err3 = db.Exec(`ALTER TABLE author ADD ranking INT DEFAULT NULL ON CONFLICT DO NOTHING;`)
	if err3 != nil {
		fmt.Println("err add column --->", err3)
	}
	num, err = res3.RowsAffected()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("affected rows ---> add", num)

	// insert some value into RANKING
	res3, err3 = db.Exec(`INSERT INTO author (firstname, lastname, ranking)
	VALUES('Farrukh', 'Yuldashboyev', 1);`)
	if err3 != nil {
		fmt.Println("err insert into author --->", err3)
	}
	num, err = res3.RowsAffected()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("affected rows ---> insert", num)
}
